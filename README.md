# Threads Exercise

This is a simple exercise for implementing Threads and Runnables in Java

## Problem Statement

1. Make a "Person" class having a name, id, address and phone number.
Make a class holding a list of persons that can search by any of the previous attributes.
It must be designed to search quickly even with a very big number of persons,
so use Maps efficiently. 
To accomplish this goal you will need a separate Map for each attribute,
and different Maps may be of different types, though they must point to the
same Person objects.
Methods that insert or delete a person must not expose their partial results
(i.e. the new Person must not be searchable by any Map until it has been
inserted in all Maps).
Tip: using synchronized blocks should be enough.

2. Adapt the previous program assuming that reads will vastly outnumber writes.
Optimize it for this case by allowing reads to proceed in parallel and only
make them wait when a write is in progress.


## Credit
https://www.reddit.com/r/javahelp/comments/3xwigo/need_some_exercise_suggestions_about_java/
-- Comment #1