package com.self.vibhor.controller;

import com.self.vibhor.dao.PersonDAO;
import com.self.vibhor.models.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public class MainController {
    @Autowired
    private PersonDAO personDAO;

    @GetMapping("/person/{id}")
    public Person getPerson(@PathVariable Long id) {
        log.info("Called GetPerson with ID: {}", id);
        return personDAO.searchById(id);
    }

    @GetMapping("/person/name={name}")
    public List<Person> getPersonByName(@PathVariable String name) {
        log.info("Called GetPersonByName with NAME: {}", name);
        return personDAO.searchByName(name);
    }

    @GetMapping("/person/phone={phone}")
    public List<Person> getPersonByPhone(@PathVariable String phone) {
        log.info("Called GetPersonByPhone with PHONE: {}", phone);
        return personDAO.searchByPhone(phone);
    }

    @PostMapping("/person")
    public Person insertPerson(@RequestBody Person person) {
        log.info("Inserting New PERSON: {} {}", person.getName(), person.getPhoneNumber());
        return personDAO.insert(person.getName(), person.getPhoneNumber());
    }

    @DeleteMapping("/person/{id}")
    public Person removePersonById(@PathVariable Long id) {
        log.info("Removing person with ID: {}", id);
        return personDAO.delete(id);
    }

    @DeleteMapping("/person/{name}/{id}")
    public Person removePersonByName(@PathVariable String name, @PathVariable Long id) {
        log.info("Removing person with NAME: {}", name);
        return personDAO.delete(name, id);
    }

    @DeleteMapping("/person/{phone}/{id}")
    public Person removePersonByPhone(@PathVariable String phone, @PathVariable Long id) {
        log.info("Removing person with PHONE: {}", phone);
        return personDAO.deleteByPhone(phone, id);
    }

    @PostMapping("/person/seed")
    public ResponseEntity<?> createDatabase() {
        personDAO.createDatabase();
        return ResponseEntity.ok("Done");
    }
}
