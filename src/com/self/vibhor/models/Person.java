package com.self.vibhor.models;

import com.sun.istack.internal.NotNull;

public class Person implements Cloneable {
    private Long id;
    private String name;
    private String phoneNumber;

    public Person clone() {
        return new Person(getId(), getName(), getPhoneNumber());
    }

    public Person(@NotNull Long id, String name, String phoneNumber) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public Person(Long id) {
        this.id = id;
        this.name = "Default";
        this.phoneNumber = "0000000000";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Person && ((Person) obj).getId() != null && ((Person) obj).getId().equals(id);
    }

    @Override
    public String toString() {
        return "Person{id=" + id + ", name=" + name + ", phone=" + phoneNumber + "}";
    }
}
