package com.self.vibhor.errors;

public class PersonException extends RuntimeException {
    public PersonException(String message) {
        super(message);
    }
}
