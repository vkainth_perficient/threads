package com.self.vibhor.config;

import com.self.vibhor.dao.PersonDAO;
import com.self.vibhor.dao.impl.PersonDAOImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InMemoryConfig {

    @Bean
    public PersonDAO personDAO() {
        return new PersonDAOImpl();
    }
}
