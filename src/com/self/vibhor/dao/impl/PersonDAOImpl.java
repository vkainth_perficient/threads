package com.self.vibhor.dao.impl;

import com.self.vibhor.dao.PersonDAO;
import com.self.vibhor.errors.PersonException;
import com.self.vibhor.models.Person;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class PersonDAOImpl implements PersonDAO {
    Map<Long, Person> idsMap = new HashMap<>();
    Map<String, List<Person>> nameMap = new HashMap<>();
    Map<String, List<Person>> phoneMap = new HashMap<>();

    Long currentId = 1L;

    @Override
    public void createDatabase() {
        log.info("Building database with 10 entries");
        String[] names = "Hello World This Is A New Kind Of Name Generator Say Nice Of My Little Friend".split("\\s+");
        for (int i = 0; i < 9; i++) {
            int randIdx = new Random().nextInt(names.length);
            long randomPhone = new Random().nextLong();
            insert(names[randIdx], Long.toString(randomPhone));
        }
    }

    @Override
    synchronized public Person searchById(Long id) {
        log.info("Searching for ID: {}", id);
        return idsMap.getOrDefault(id, null);
    }

    @Override
    synchronized public List<Person> searchByName(String name) {
        log.info("Searching for NAME: {}", name);
        return nameMap.getOrDefault(name, new ArrayList<>());
    }

    @Override
    synchronized public List<Person> searchByPhone(String phone) {
        log.info("Searching for PHONE: {}", phone);
        return phoneMap.getOrDefault(phone, new ArrayList<>());
    }

    @Override
    synchronized public void insert(Person person) {
        Long personId = person.getId();
        String personName = person.getName();
        String personPhone = person.getPhoneNumber();

        log.info("Inserting {}", person.toString());

        idsMap.put(personId, person.clone());
        updateMap(person, personName, nameMap);
        updateMap(person, personPhone, phoneMap);

        log.info("Done inserting");
    }

    synchronized private void updateMap(Person person, String key, Map<String, List<Person>> map) {
        if (map.containsKey(key)) {
            map.get(key).add(person.clone());
        } else {
            List<Person> newList = new ArrayList<>();
            newList.add(person.clone());
            map.put(key, new ArrayList<>(newList));
        }
    }

    @Override
    synchronized public Person insert(String name, String phone) {
        Person person = new Person(currentId++, name, phone);
        insert(person);
        return person;
    }

    @Override
    synchronized public Person insert(String name) {
        Person person = new Person(currentId++, name, "0000000000");
        insert(person);
        return person;
    }

    @Override
    synchronized public Person delete(Long id) {
        if (idsMap.containsKey(id)) {
            Person found = idsMap.get(id);
            idsMap.remove(id);
            return found;
        }
        throw new PersonException("ID does not exist: " + id);
    }

    @Override
    synchronized public Person delete(String name, Long id) {
        List<Person> identified = searchByName(name);
        if (identified != null && identified.size() > 0) {
            Person removed = delete(id);
            nameMap.get(name).remove(removed);
            phoneMap.get(removed.getPhoneNumber()).remove(removed);
            return removed;
        }
        throw new PersonException("NAME does not exist: " + name);
    }

    @Override
    public Person deleteByPhone(String phone, Long id) {
        List<Person> identified = searchByPhone(phone);
        if (identified != null && !identified.isEmpty()) {
            Person removed = delete(id);
            nameMap.get(removed.getName()).remove(removed);
            phoneMap.get(phone).remove(removed);
            return removed;
        }
        throw new PersonException("PHONE does not exist: " + phone);
    }
}
