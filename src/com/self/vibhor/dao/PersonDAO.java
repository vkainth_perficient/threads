package com.self.vibhor.dao;

import com.self.vibhor.models.Person;

import java.util.List;

public interface PersonDAO {
    void createDatabase();

    Person searchById(Long id);

    List<Person> searchByName(String name);

    List<Person> searchByPhone(String phone);

    void insert(Person person);

    Person insert(String name, String phone);

    Person insert(String name);

    Person delete(Long id);

    Person delete(String name, Long id);

    Person deleteByPhone(String phone, Long id);
}
